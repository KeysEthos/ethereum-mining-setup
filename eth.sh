#!/bin/sh


# Color Spec 
red=`tput setaf 1`
blue=`tput setaf 4`
green=`tput setaf 2`
purple=`tput setaf 5`
end=`tput sgr0`

echo "${blue} Select Package to Install${end}"
echo "1) Ethereum Get Package"
echo "2) Claymore Mining Setup"
echo "3) Start Mining"

# Choicevar selection 
read -p "${purple} Selection: ${end}" choicevar

# Options Run 
if [ "$choicevar" = "1" ]
then
	printf "${blue} Installing Ethereum Libararies ${end}"
	sudo add-apt-repository ppa:ethereum/ethereum
	sudo apt-get update
	sudo apt-get install -y ethereum geth
	printf "\n\n ${green} Base Ethereum and Geth Package installed ${end} \n\n"

elif [ "$choicevar" = "2" ] 
then
	printf "${blue} Installing Claymore Miner ${end}"
	cd ~/Downloads
	sudo apt install -y curl
	curl -L -o claymore_10.2_miner.tar.gz https://drive.google.com/uc?id=1t25SK0lk2osr32GH623rR8aG2_qvZds9
	sudo mkdir /usr/local/claymore10.2
	sudo tar -xvf claymore_10.2_miner.tar.gz -C /usr/local/claymore10.2
	cd /usr/local/claymore10.2
	cd Clay*
	sudo mv * /usr/local/claymore10.2
	cd ..
	sudo rm -r Clay*
	sudo chown root:root ethdcrminer64
	sudo chmod 755 ethdcrminer64
	sudo chmod u+s ethdcrminer64
	cd
	sudo cp block/mine.sh /usr/local/claymore10.2/mine.sh
	cd /usr/local/claymore10.2
	sudo chmod +x mine.sh
	printf "\n\n ${green} Claymore Miner Installed ${end} \n\n"
	printf "\n\n ${green} Ready to Start Claymore Minder ${end} \n\n"
elif [ "$choicevar" = "3" ]
then 
	printf "${green} Starting Mining Script ${end}"
	cd /usr/local/claymore10.2 
	./mine.sh
	printf "${green} Miner Starting ${end}"

# catch all non-response
else 
	printf "${red} $choicevar doesn't match options ${end}"
fi	
	
